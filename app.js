const express            = require('express');
const bodyParser         = require('body-parser');
const fileupload         = require('express-fileupload');
//LOGS
const logger             = require('./settings/Logger');
//ROUTES
const routes             = require('./routes/index');
//APP
const app                = express();
//MIDDLEWARES
app.use(fileupload());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//CORS
const cors = require('cors');
//ENV
require('dotenv').config()
//SETTINGS
const port              = process.env.SERVER_PORT || 3000;
const settings          = require("./settings/config")
const mysql             = require('mysql');
const db = mysql.createConnection (settings.ConnectionString());

db.connect(function(err) {
  if (err) {
    logger.error("app.js", err);   
    throw err;
  }
  console.log("Connected!");
});
//GLOBALS
global.db = db;

app.set('port', port);

//Middleware - API GATEWAY
/* const GatewayMiddleware = require('@fabrikperu/gateway_middleware');
const GatewayConstants = require('@fabrikperu/gateway_middleware/constants');
let gateway_middleware = new GatewayMiddleware(GatewayConstants.COMPETITION);
app.use((req,res,next) => {
    gateway_middleware.handler(req,res,next);
}); 
 */
app.use(cors());
//ROUTES
app.use(routes);
//SERVER
app.listen(port, () =>{
    console.log("Start server on port " + port)
})
//se agrewgo para MOCHA
module.exports = app