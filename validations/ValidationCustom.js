//common files inside constants folder for each error message in app
var messages                  = require('../constants/messages'); 
//create instance of express-validator
const {check}                 = require('express-validator');
//const {matchedData, sanitize} = require('express-validator/filter');
const {matches, sanitizeBody} = require('express-validator');

function createValidationFor(route) {
    switch (route) {
        case 'receiptCreateForOne':
            return [                
                check('assigned_month').not().isEmpty().withMessage(messages.INVALID_REQUIRED),
                check('assigned_year').not().isEmpty().withMessage(messages.INVALID_REQUIRED), 
                check('user_id_member').not().isEmpty().withMessage(messages.INVALID_REQUIRED),
                check('user_id_creator').not().isEmpty().withMessage(messages.INVALID_REQUIRED),
            ];
        case 'receiptCreateForAll':
            return [                
                check('assigned_month').not().isEmpty().withMessage(messages.INVALID_REQUIRED),
                check('assigned_year').not().isEmpty().withMessage(messages.INVALID_REQUIRED), 
                check('user_id_creator').not().isEmpty().withMessage(messages.INVALID_REQUIRED),
            ];
        case 'receiptUpdate':
            return [                                
                check('title').not().isEmpty().withMessage(messages.INVALID_REQUIRED)     
                //.matches(/\s/).withMessage('must contain a number'),
                //sanitizeBody('notifyOnReply').toBoolean()                
            ];
        case 'receiptUpdateFile':
            return [
                check('user_id').not().isEmpty().withMessage(messages.INVALID_REQUIRED)
            ];  
            
        default:
            return [];
    }
} 

exports.createValidationFor = createValidationFor;