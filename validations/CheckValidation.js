var response = require('../constants/response'); //common function for every http response inside constants folder
//create instance of express-validator
const {validationResult} = require('express-validator');
//const {matchedData, sanitize} = require('express-validator/filter');


module.exports.CheckValidationResult = CheckValidationResult;
//function that validate express-validator results
function CheckValidationResult(req, res, next) {
  /*   var result = validationResult(req);
    if (!result.isEmpty()) {
         response.CustomResponse(res, 400, result.array()[0].msg, result, {}); //response.CustomResponse is a method of constants/MyResponse
        return res.status(422).jsonp(errors.array());


    } else {
        next(); //go ahead if request is valid
    }
 */
    
    const result = validationResult(req);
    if (result.isEmpty()) {
        return next();
    }

    res.status(422).json({ errors: result.array() });



}
