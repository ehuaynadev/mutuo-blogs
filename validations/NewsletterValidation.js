var messages = require('../constants/messages'); //common files inside constants folder for each error message in app

//create instance of express-validator
const {check}  = require('express-validator');
//const {matchedData, sanitize} = require('express-validator/filter');

//function that actually validate request
/* module.exports.ValidateReqArticlesCreate = [
    check('title').isEmail().withMessage('title' + messages.INVALID_REQUIRED),
    check('description').not().isEmpty().withMessage('description' + messages.INVALID_REQUIRED),
    check('image').not().isEmpty().withMessage('image' + messages.INVALID_REQUIRED),
];
 */

module.exports.ValidateReqNewsletterUpdate = [
    check('title').isEmail().withMessage('title' + messages.INVALID_REQUIRED)
];


