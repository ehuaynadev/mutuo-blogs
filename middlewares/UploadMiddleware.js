 const multer  = require('multer');
 const path= require("path");
/*
const upload = multer({
    limits: {
      fileSize: 4 * 1024 * 1024,
    }, 
    storage : multer.diskStorage({
        destination : function(req, file, next){
            next(null, './public/images');
        },
        filename : function(req, file, next){
            const ext = file.mimetype.split("/")[1];
            next(null, file.fieldname + '-'+ Date.now() + '.' + ext);
      
        }
    }), 
    fileFilter: function(req, file, next){
        if(!file){
            next();
        }

        const image = file.mimetype.startsWidth('image/');
        if(image){
            next(null, true);
        }else{
            next({message: "filetype not supported"}, false);
        }
    }   
}); */

/* var upload = multer({
    fileFilter: (req, file, cb) => {
      if (file.mimetype !== 'image/jpeg') {
        return cb(new Error('Only jpeg images allowed'))
      }
  
      cb(null, true)
    }
  });

var avatarUpload = upload.single('photo');
 */

const upload = multer({
    //dest: `${FILE_PATH}/`
    fileFilter: function (req, file, callback) {
      var ext = path.extname(file.originalname);
      if(ext !== '.pdf') {
          return callback(new Error('Only PDF are allowed'))
      }
      callback(null, true)
  },
});

//var avatarUpload = upload.single('photo')



module.exports = upload;













/* 

Use express validator middleware after the multer that's why express validator work naturally

This example work for me , try this

import require dependencies

var fs = require('fs');
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
configure multer

var obj = {};
var diskStorage = multer.diskStorage({

    destination:function(req,file,cb){

        var dest = path.join(__dirname,'/uploads');

        mkdirp(dest,function(err){
            if(err) cb(err,dest);
            else cb(null,dest);
        });
    },
    filename:function(req,file,cb){
        var ext = path.extname(file.originalname);
        var file_name = Date.now()+ext;
        obj.file_name = file_name;
        cb(null,file_name);
    }
});
var upload = multer({storage:diskStorage}); */