const BlogCategoryController = {};

const queries        = require('../constants/queries'); 
const constants      = require('../constants/constants');
const helpers        = require('../helpers/helper');

const Response       = require('../utils/Response');
const response       = new Response();

const logger         = require('../settings/Logger');


const utilFormat     = require('util'); 

BlogCategoryController.List = async (req, res) => {

    try{

     
      
        db.query(queries.SQLBlogCategoriesList, async (err, result) => {
                if(err){                   
                    response.setError(500, constants.CodeBadRequest, err);
                    return response.send(res);       
                }           

              
                response.setSuccess(200, constants.CodeOK, '',result);
                return response.send(res); 
        });  

    }catch(err){   
        response.setError(400, err);
        return response.send(res);
    }   
}



module.exports = BlogCategoryController;


