const BlogController = {};

const queries        = require('../constants/queries'); 
const constants      = require('../constants/constants');
const helpers        = require('../helpers/helper');

const Response       = require('../utils/Response');
const response       = new Response();

const logger         = require('../settings/Logger');


const utilFormat     = require('util'); 

BlogController.List = async (req, res) => {

    try{

        let pageIn              = typeof req.query.page !== 'undefined' ? req.query.page : -1;
        const hasPagination     = pageIn !== -1;
        let perPageIn           = typeof req.query.per_page !== 'undefined' ? req.query.per_page : (hasPagination ? 0 : 5);
        let start_index         = (pageIn - 1) * perPageIn;
       
        let query               = queries.SQLBlogListPublic;

        if(hasPagination) {
            query = utilFormat.format(query +` limit %d, %d`, start_index, perPageIn);           
        }
      
        db.query(query, async (err, result) => {
                if(err){   
                    logger.error("SQLBlogListPublic", err);               
                    response.setError(500, constants.CodeBadRequest, err);
                    return response.send(res);       
                }           

                for(var i=0; i < result.length; i++){                            
                    result[i].photo_generated         = process.env.URLIMAGE + result[i].photo_generated;
                }  
               
                var total = await helpers.getFromPromise(queries.SQLBlogCountPublic)
                .then(result => {
                    return result[0].count;
                })
                .catch(err => {
                    logger.err("SQLBlogCountPublic catch", err);
                    response.setError(500, constants.CodeBadRequest, err);
                    return response.send(res);    
                });

                let pages = 1;
                
                if (total >= perPageIn)
                     pages = Math.ceil(total / perPageIn); 
                     
 
                 let _result = {                  
                     total_records: total
                 };
 
                 if(hasPagination) {
                    _result.page        = parseInt(pageIn);
                    _result.per_page    = parseInt(perPageIn);          
                    _result.total_pages = pages;                            
                }
         
                _result.rows = result;
      
                response.setSuccess(200, constants.CodeOK, 'Blogs List',_result);
                return response.send(res); 
        });  

    }catch(err){
        logger.error("BlogController.Show catch", err);
        response.setError(400, err);
        return response.send(res);
    }   
}


BlogController.Show = async (req, res) => {

    try{

        let blogIDIn             = req.params.blog_id;
      
        db.query(queries.SQLBlogShowPublic, blogIDIn, async (err, result) => {
            if(err){
               logger.error("BlogController.Show SQLBlogShowPublic", err);
               response.setError(500, constants.CodeBadRequest, err);
               return response.send(res);            
            }          
            
            numRows = result.length;

            if (numRows != 0){       
                
                for(var i=0; i < result.length; i++){                            
                    result[i].photo_generated         = process.env.URLIMAGE + result[i].photo_generated;

                    var next = await helpers.getFromPromiseNew(queries.SQLBlogNextPublic, blogIDIn)
                    .then(result => {
                        console.log(result);
                        return result;
                    })
                    .catch(err => {
                        return res.status(500).json({ "code": constants.CodeBadRequest, "message": err }); 
                    });
                    
                    var prev = await helpers.getFromPromiseNew(queries.SQLBlogPrevPublic, result[i].blog_id)
                    .then(result => {
                        return result;
                    })
                    .catch(err => {
                        return res.status(500).json({ "code": constants.CodeBadRequest, "message": err }); 
                    }); 

                    let nextResult = next.length>0 ? next[0].blog_id : null;
                    let prevResult = prev.length>0 ? prev[0].blog_id : null;
  
                    result[i].prev = prevResult;
                    result[i].next = nextResult;
 
                }  

                response.setSuccess(200, constants.CodeOK, 'Blogs Show', result[0]);
                return response.send(res);

            } else{
                response.setSuccess(200, constants.CodeOK, 'No existe el registro',{});
                return response.send(res); 
            }                
        });  

    }catch(err){
        logger.error("BlogController.Show catch", err);
        response.setError(400, err);
        return response.send(res);          
    }   
}

module.exports = BlogController;


