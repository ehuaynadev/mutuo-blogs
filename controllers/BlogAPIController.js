const BlogController = {};

const queries        = require('../constants/queries'); 
const constants      = require('../constants/constants');
const utilities      = require('../utils/utilities');
const helpers        = require('../helpers/helper');
const servicesAWS    = require('../helpers/AWS');

const Response       = require('../utils/Response');
const response       = new Response();

const logger         = require('../settings/Logger');

const utilFormat     = require('util'); 
const bcrypt         = require('bcrypt');

BlogController.Create = async (req, res) => {

    try{

        let titleIn                     = req.body.title,
            userCreatorID               = req.body.user_creator_id,            
            descriptionIn               = req.body.description,
            stateIn                     = req.body.state,
            blogCategoryID              = req.body.blog_category_id

        let filePhotoOriginalName       = req.files.file_photo.name,
            filePhotoOriginallData      = req.files.file_photo.data,
            filePhotoGeneratedName      = utilities.RandomFileName(filePhotoOriginalName);

        let publishedAt;
        let createdAt;

        if(stateIn==="BORRADOR"){
            publishedAt =  null;
          
        }else if("PUBLICADO"){            
            publishedAt =  utilities.GetDatetimeCurrent();            
        }

       
        let data = [
            titleIn,
            descriptionIn,                 
            filePhotoOriginalName,      
            filePhotoGeneratedName,      
            userCreatorID,      
            stateIn,  
            utilities.GetDatetimeCurrent(),
            publishedAt, 
            blogCategoryID,
        ];

   
    
        db.query(queries.SQLBlogCreate, data, async function(err, result) {
            if (err) { 
                    logger.error("SQLBlogCreate", err);
                    response.setError(500, constants.CodeBadRequest, err);
                    return response.send(res);       
            }

            if(result.affectedRows){

                let lastBlogID = result.insertId;      
                let BlogCreatedRes = {};

                BlogCreatedRes.blog_id       = lastBlogID;
                BlogCreatedRes.title         = titleIn;
                BlogCreatedRes.description   = descriptionIn;

                console.log(result);

                servicesAWS.uploadObjectToIBM("bucket-mutuo", filePhotoGeneratedName, filePhotoOriginallData);  
                response.setSuccess(201, constants.CodeOK, result.message, BlogCreatedRes);                

            } else{                
                response.setSuccess(200, constants.CodeOK, 'No se ha creado el registro', _result);                
            }

            return response.send(res);
    
        }); 

    }catch(err){
        logger.err("BlogController.Create catch", err);
        response.setError(400, constants.CodeBadRequest, err);
        return response.send(res);       
    }   
}

BlogController.List = async (req, res) => {

    try{

        let pageIn              = typeof req.query.page !== 'undefined' ? req.query.page : -1;
        const hasPagination     = pageIn !== -1;
        let perPageIn           = typeof req.query.per_page !== 'undefined' ? req.query.per_page : (hasPagination ? 0 : 5);
        let start_index         = (pageIn - 1) * perPageIn;
       
        let query               = queries.SQLBlogList;

        if(hasPagination) {
            query = utilFormat.format(query +` limit %d, %d`, start_index, perPageIn);           
        }
      
        db.query(query, async (err, result) => {
                if(err){              
                    logger.error("SQLBlogList", err);    
                    response.setError(500, constants.CodeBadRequest, err);
                    return response.send(res);       
                }           

                for(var i=0; i < result.length; i++){                            
                    result[i].photo_generated         = process.env.URLIMAGE + result[i].photo_generated;
                }  
               
                var total = await helpers.getFromPromise(queries.SQLBlogCount)
                .then(result => {
                    return result[0].count;
                })
                .catch(err => {
                    logger.error("SQLBlogCount", err);                        
                    response.setError(500, constants.CodeBadRequest, err);
                    return response.send(res);   
                });

                let pages = 1;
                
                if (total >= perPageIn)
                     pages = Math.ceil(total / perPageIn); 
                     
 
                let _result = {                  
                     total_records: total
                 };
 
                 if(hasPagination) {
                    _result.page        = parseInt(pageIn);
                    _result.per_page    = parseInt(perPageIn);          
                    _result.total_pages = pages;                            
                }
         
                _result.rows = result;

                response.setSuccess(200, constants.CodeOK, 'Blogs List', _result);
                return response.send(res);
        });  

    }catch(err){
        logger.err("BlogController.List catch", err);
        response.setError(400, constants.CodeBadRequest, err);
        return response.send(res);       
    }   
}


BlogController.ListByCompetition = async (req, res) => {

    try{

        let competitionID                 = req.params.competitionID;
      
        db.query(queries.SQLBlogListByCompetition, competitionID, async (err, result) => {
                if(err){
                    logger.error("SQLBlogListByCompetition", err);                                                         
                    response.setError(500, constants.CodeBadRequest, err);
                    return response.send(res);   
                }           

                for(var i=0; i < result.length; i++){                            
                    result[i].photo_generated         = process.env.URLIMAGE + result[i].photo_generated;
                }  

                response.setSuccess(200, constants.CodeOK, 'Blogs ListByCompetition', _result);
                return response.send(res);
        });  

    }catch(err){
        logger.err("BlogController.ListByCompetition catch", err);
        response.setError(400, constants.CodeBadRequest, err);
        return response.send(res);       
    }   
}

BlogController.Show = async (req, res) => {

    try{

        let blogIDIn             = req.params.blog_id;
      
        db.query(queries.SQLBlogShow, blogIDIn, async (err, result) => {
            if(err){
                logger.error("SQLBlogShow", err);                    
                response.setError(500, constants.CodeBadRequest, err);
                return response.send(res);   
            }          
            
            numRows = result.length;

            if (numRows > 0){       
                
                for(var i=0; i < result.length; i++){                            
                    result[i].photo_generated         = process.env.URLIMAGE + result[i].photo_generated;
                }  
            
                response.setSuccess(200, constants.CodeOK, 'Blogs Show', result[0]);
                return response.send(res);

            } else{                
                response.setSuccess(204, constants.CodeOK, {});
                return response.send(res); 
            }                
        });  

    }catch(err){
        logger.err("BlogController.Show catch", err);
        response.setError(400, constants.CodeBadRequest, err);
        return response.send(res);    
    }   
}

BlogController.Update = async (req, res) => {

    try{

        let blogIDIn             = req.params.blog_id;

        let title                = req.body.title,
            description          = req.body.description;   
            blogCategoryID       = req.body.blog_category_id;     
     
        let data = [
            title,
            description,
            parseInt(blogCategoryID),                 
            blogIDIn,  
            
        ]

        console.log(data);
        
      
        db.query(queries.SQLBlogUpdate, data, (err, result) => {
            if(err){                        
                logger.error("SQLBlogUpdate", err);                
                console.log(err);  
                response.setError(500, constants.CodeBadRequest, err);
                return response.send(res);   
            }          

            console.log(result);
            
            if(result.affectedRows){             
                response.setSuccess(200, constants.CodeOK, 'Se realizò cambios', {});
            } else{    

                response.setSuccess(200, constants.CodeOK, 'No se realizaron cambios', {});                
            }   
            return response.send(res);             
        });  

    }catch(err){
        logger.err("BlogController.Update catch", err);
        response.setError(400, constants.CodeBadRequest, err);
        return response.send(res);        
    }   
}

BlogController.UpdatePhoto = async (req, res) => {

    try{

      
        let blogIDIn             = req.params.blog_id;
        let archivo              = req.files;
   
        if(archivo){

            


            let fileType = req.files.file_photo.mimetype;

            if(fileType == "image/jpeg" || fileType == "image/jpg" || fileType == "image/png"){

                let filePhotoOriginalName   = req.files.file_photo.name,
                filePhotoOriginallData      = req.files.file_photo.data,
                filePhotoGeneratedName      = utilities.RandomFileName(filePhotoOriginalName);       
    
                let data = [
                    filePhotoOriginalName,
                    filePhotoGeneratedName,    
                    blogIDIn, 
                ];
                
                let query              = utilFormat.format(queries.SQLBlogGetOne, blogIDIn);                
                
                db.query(query, async (err, resultGetOne) => {
                    if(err){                                                
                        logger.error("SQLBlogGetOne", err);                                                                                 
                        response.setError(500, constants.CodeBadRequest, err);
                        return response.send(res);    
                    }      
                    
                    if(resultGetOne.length>0){
            
                        db.query(queries.SQLBlogUpdatePhoto, data, async (err, result) => {
                            if(err){                        
                                logger.error("SQLBlogUpdatePhoto", err);                                                       
                                return res.status(500).json({ "code": constants.CodeBadRequest, "message": err });
                            }      
                            
                            if(result.affectedRows){
                
                                servicesAWS.deleteObjectToIBM("bucket-mutuo", resultGetOne[0].photo_generated);                     
                                servicesAWS.uploadObjectToIBM("bucket-mutuo", filePhotoGeneratedName, filePhotoOriginallData);    
                                
                                response.setSuccess(200, constants.CodeOK, result.message, {});
                            }else{                
                                response.setSuccess(200, constants.CodeOK, 'No se ha realizado la actualizacion', {});                                
                            }                
                            return response.send(res);     
                        });   
                    }
                });  
         
            }else{             
                response.setSuccess(200, constants.CodeOK, 'Sólo se pueden subir imàgenes en formato JPG, PNG', {});                                
                return response.send(res);    
            }

        }else{

            response.setSuccess(200, constants.CodeOK, '', {});                                
            return response.send(res);    
            /* res.status(200).json({ "code": constants.CodeOK, "message": "Debe ingresar una imagen" });
            return response.send(res);     */
        }

        

    }catch(err){        
        logger.err("BlogController.UpdatePhoto catch", err);
        response.setError(400, constants.CodeBadRequest, err);
        return response.send(res);    
    }   
}

BlogController.UpdateState = async (req, res) => {

    try{

        let blogIDIn             = req.params.blog_id;
        let stateIn              = req.body.state;
 
        let data = [
            stateIn,
            utilities.GetDatetimeCurrent(),
            blogIDIn,
        ]
      
        db.query(queries.SQLBlogUpdateState, data, async (err, result) => {
            if(err){                        
                logger.error("SQLBlogUpdateState", err);                                                                                 
                response.setError(500, constants.CodeBadRequest, err);
                return response.send(res);    
            }          
            
            if(result.affectedRows){     
                response.setSuccess(200, constants.CodeOK, result.message,{});
            } else{               
                response.setSuccess(200, constants.CodeOK, 'No hay registros con el ID ingresado',{});
                
            }          
            return response.send(res);       
        });  

    }catch(err){
        logger.err("BlogController.UpdateState catch", err);
        response.setError(400, constants.CodeBadRequest, err);
        return response.send(res);    
    }   
}


BlogController.Delete = async (req, res) => {

    try{

        let blogs             = req.body.blogs;
      
        for (x=0; x< blogs.length; x++){
            
            
            let currentBlogID     = blogs[x].blog_id;
            let query              = utilFormat.format(queries.SQLBlogGetOne, currentBlogID);

            var getRows = await helpers.getFromPromise(query)            
            .then(result => {

                if(result.length > 0){
                 
                    let currentBlog         = result[0].photo_generated;                   
                    let queryDelete         = utilFormat.format(queries.SQLBlogDelete, currentBlogID);        
           
                    db.query(queryDelete, (err, result) => {
                        if (err) {
                            logger.error("SQLBlogDelete", err);                         
                            response.setError(500, constants.CodeBadRequest, err);
                            return response.send(res);    
                        }             
    
                        servicesAWS.deleteObjectToIBM("bucket-mutuo", currentBlog);     
                        
                    })  
                    
                }else{
                    console.log("no existe");
                }

                return result;

            })
            .catch(err => {
                logger.error("SQLBlogGetOne", err);         
               // logger.info(err);                        
            });  
            
        }

        response.setSuccess(200, constants.CodeOK, 'Se ha eliminado el registro',{});
        return response.send(res); 

    }catch(err){
        logger.err("BlogController.Delete catch", err);
        response.setError(400, constants.CodeBadRequest, err);
        return response.send(res);    
    }   
}
 
module.exports = BlogController;


