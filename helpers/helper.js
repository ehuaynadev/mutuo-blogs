async function getFromPromise(sql) {

    return new Promise((resolve, reject) => {
        db.query(sql, (err, res) => {
          if (err) {
            return reject(err);
          }
          return resolve(res);
        });
      });
}

async function getFromPromiseNew(sql, data) {

  return new Promise((resolve, reject) => {
      db.query(sql, data, (err, res) => {
        if (err) {
          return reject(err);
        }
        return resolve(res);
      });
    });
}

exports.getFromPromise = getFromPromise;
exports.getFromPromiseNew = getFromPromiseNew;
