const settings          = require("../settings/AWS")
const config            = settings.ConnectionAWS();
const AWS               = require('ibm-cos-sdk');

function uploadObjectToIBM(bucketName, itemName, fileData) {

    const cos    = new AWS.S3(config);

    console.log(`Creating new item: ${itemName}`);
    return cos.putObject({
        Bucket: bucketName,
        Key: itemName,
        ContentDisposition: 'inline',
        //ContentType:'application/pdf',
        Body: fileData
    }).promise()
    .then(() => {
        console.log(`Item: ${itemName} created!`);
    })
    .catch((e) => {
        console.error(`ERROR: ${e.code} - ${e.message}\n`);
    });
}

function deleteObjectToIBM(bucketName, itemName) {

    const cos         = new AWS.S3(config);

    console.log(`Deleting item: ${itemName}`);
    return cos.deleteObject({
        Bucket: bucketName,
        Key: itemName
    }).promise()
    .then(() =>{
        console.log(`Item: ${itemName} deleted!`);
    })
    .catch((e) => {
        console.error(`ERROR: ${e.code} - ${e.message}\n`);
    });
}
 
exports.uploadObjectToIBM = uploadObjectToIBM;
exports.deleteObjectToIBM = deleteObjectToIBM;


