//var assert = require('assert');
var supertest = require('supertest');
/*
var app = require('../app');
//var pg = require('../lib/postgres');
//var pg = require('../settings/config.js');

let chai = require("chai");
let chaiHttp = require("chai-http");
//let server=require("../app");
let should = chai.should();
chai.use(chaiHttp);











var mysql = require('mysql');

 */


//var assert = require("assert");
let chai = require("chai");
let chaiHttp = require("chai-http");
let server=require("../app");
//let server = supertest.agent("http://localhost:8080");
let should = chai.should();


chai.use(chaiHttp);
describe("Books", function(){
   /*  describe ("DELETE ALL", function(){
        it("should remove all first", done=>{
            console.log ("Deleting all data in db first.")
            chai.request(server)
                .delete("/books/")
                .send({})
                .end((err,res)=>{
                    //console.log (res)
                    // console.log("err",err);
                    res.should.have.status(200);
                    console.log("Response Body:", res.body);
                    // console.log (result);
                    done()
                })
        })

    })
    
 */
    describe ("CRUD OPERATIONS", function(){
 
        var newsletters = [{
            "newsletters_id": "121212",
            "title": "World Is Best",
            "author": "Larry",
            "year": "2016",
            "user_id": "002"

        }, {
            "newsletters_id": 11,
            "title": "totopos58",
            "author": "John",
            "year": "2016",
            "user_id": "002"
        }]


        

        /*
       it("Should add Books in DB", (done) => {
            for (book in books) {
                chai.request(server)
                    .post("/books/")
                    .send(books[book])
                    .end((err, res) => {
                        res.should.have.status(200);
                        console.log("Response Body:", res.body);
                        
                    })
            }
            done()
        }) */
    
        it ("Show all Newsletters", (done) => {
            chai.request(server)
                .get("/newsletters?current_page=1&items_per_page=10")
                .end((err, result)=>{
                    result.should.have.status(200);
                    console.log ("- Got",result.body.data.total_records, " newsletters")
                    //console.log ("* Got",result.body.data.total_records, "  newsletters")
                    console.log ("Result Body:", result.body);                    
                    done()
                })
        })

        it ("Show Particular Newsletters only", (done)=>{
            chai.request(server)
                .get("/newsletters/" + newsletters[1].newsletters_id)
                .end((err, result)=>{                    
                    result.should.have.status(200)
                    console.log("Fetched Particlar newsletter using /GET/newsletters/:newsletter_id ::::", result.body)
                    done()
                })
        }) 
 
        it ("Should Update Partcular Book Only", (done)=>{
            var updateNewsletter = {
                "title": "totopos58"
            }
            
            chai.request(server)
                .put("/newsletters/" + newsletters[1].newsletters_id + "/users/" + newsletters[1].user_id)
                .send(updateNewsletter)
                .end((err, result)=>{                    
                    result.should.have.status(200)
                    console.log("Updated Particlar newsletter using /PUT/newsletters/:newsletter_id ::::", result.body) 
                    done()
                })
        }) 

        it ("should check data updated in DB", (done)=>{
            chai.request(server)
                .get("/newsletters/" + newsletters[1].newsletters_id)
                .end((err, result)=>{                    
                    result.should.have.status(200)                
                    //result.body.data.year.should.eq("2017")     
                    console.log("Fetched Particlar newsletter using /GET/newsletters/:newsletter_id ::::", result.body)    
                    done()
                })
        })  

        it("Should Delete Particular newsletters", (done) => {

            var newslettersData = {
                "user_id": "001",
                "newsletters": [{
                    "newsletter_id":13
                }]
            }

            chai.request(server)
                    .delete("/newsletters")
                    .send(newslettersData)
                    .end((err, result)=>{              
                        //el resultado debe tener resultado 200      
                        result.should.have.status(200)                
                        console.log("Deleted MULTIPLE newsletter using /DELETE/newsletters/:newsletter_id ::::", result.body)    
                        done()
            })
        }) 

        /* it("Should confirm delete with number of Docs from DB", (done)=>{
            chai.request(server)
                .get("/books/")
                .end((err, result)=>{
                    result.should.have.status(200);
                    result.body.data.length.should.eq(1);
                    console.log ("Got",result.body.data.length, " docs")
                    //console.log ("Result Body:", result.body);
                    done()
                })
        }) */

    })
})