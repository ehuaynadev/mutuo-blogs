describe ("CRUD OPERATIONS", function(){

        var books = [{
            "isbn": "121212",
            "title": "World Is Best",
            "author": "Larry",
            "year": "2016"

        }, {
            "isbn": "121213",
            "title": "Node JS",
            "author": "John",
            "year": "2016"

        }]
        it("Should add Books in DB", (done) => {
            for (book in books) {
                chai.request(server)
                    .post("/books/")
                    .send(books[book])
                    .end((err, res) => {
                        res.should.have.status(200);
                        console.log("Response Body:", res.body);
                        
                    })
            }
            done()
        })
   //...     
 })