it("Should Delete Particular Book", (done)=>{
    chai.request(server)
        .delete("/books/"+books[1].isbn)
        .end((err, result)=>{                    
            result.should.have.status(200)                
            console.log("Deleted Particlar Book using /GET/BOOKS/:BOOKID ::::", result.body)    
            done()
        })
})

it("Should confirm delete with number of Docs from DB", (done)=>{
    chai.request(server)
        .get("/books/")
        .end((err, result)=>{
            result.should.have.status(200);
            result.body.data.length.should.eq(1);
            console.log ("Got",result.body.data.length, " docs")
            //console.log ("Result Body:", result.body);
            done()
        })
})