    module.exports = {
 
        
        //PUBLIC
        
        SQLBlogListPublic       : `SELECT b.blog_id, b.title, b.photo_generated, b.created_at, b.publication_date, b.state, 
        bc.blog_category_id, bc.name as blog_category_name FROM blogs as b
        INNER JOIN blog_categories  as bc
        on b.blog_category_id = bc.blog_category_id
        where b.state='PUBLICADO'
        ORDER BY b.blog_id DESC`,

        SQLBlogShowPublic       : `SELECT b.blog_id, b.title, b.description, b.photo_generated, b.created_at, b.publication_date, b.state, 
        bc.blog_category_id, bc.name as blog_category_name FROM blogs as b
        INNER JOIN blog_categories  as bc
        on b.blog_category_id = bc.blog_category_id
        where b.blog_id = ? and b.state='PUBLICADO'`,

        SQLBlogCountPublic      : `SELECT count(blog_id) as count  from blogs WHERE state='PUBLICADO'`,

        SQLBlogNextPublic       : `select blog_id from blogs where blog_id = (select max(blog_id) from blogs where blog_id < ? and state='PUBLICADO')`,

        SQLBlogPrevPublic       : `select blog_id from blogs where blog_id = (select min(blog_id) from blogs where blog_id > ? and state='PUBLICADO')`,



        //API
        
        SQLBlogCreate           : `INSERT INTO blogs( title,                                                  
                                                  description,
                                                  photo_original,
                                                  photo_generated,
                                                  user_creator_id,
                                                  state,
                                                  created_at,
                                                  publication_date, 
                                                  blog_category_id) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ? )`,

        SQLBlogDelete           : `DELETE FROM blogs WHERE blog_id = %d`,

        SQLBlogGetOne           : `SELECT blog_id, photo_generated from blogs WHERE blog_id = %d`,        

        SQLBlogUpdate           : `UPDATE blogs
                                   SET  title = ?,
                                        description = ?,
                                        blog_category_id = ?
                                   WHERE blog_id = ?`,
        
        SQLBlogUpdatePhoto      : `UPDATE blogs
                                   SET photo_original  = ?,
                                       photo_generated = ?	
                                   WHERE blog_id = ?`,

        SQLBlogUpdateState      : `UPDATE blogs
                                   SET state  = ?,
                                       publication_date = ?
                                   WHERE blog_id = ?`,
         
         SQLBlogList            : `SELECT b.blog_id, b.title, b.photo_generated, b.created_at, b.publication_date, b.state, 
         bc.blog_category_id, bc.name as blog_category_name FROM blogs as b
         INNER JOIN blog_categories  as bc
         on b.blog_category_id = bc.blog_category_id
         ORDER BY b.blog_id DESC`,

         SQLBlogCount           : `SELECT count(blog_id) as count  from blogs`,



        /**
        * BLOG CATEGORIES
        * */
        SQLBlogCategoriesList       : `SELECT blog_category_id, name FROM blog_categories`, 

    }
    
    