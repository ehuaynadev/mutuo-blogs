exports.CustomResponse = CustomResponse; 

function CustomResponse(res, status, message, data, pager)
{
    return res.status(status).send({
        'status' : status,
        'errors' : data.errors
    });
}