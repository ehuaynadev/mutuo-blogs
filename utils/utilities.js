const moment = require('moment-timezone');

function RandomName(length){

   var result           = '';
   //var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var characters       = '123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

function RandomFileName(fileIn){

   var extensionFile       = fileIn.split('.').pop();
   var randomName          = RandomName(30);
   var imageNameGenerated  = randomName + "." + extensionFile;

   return imageNameGenerated;
}

function GetDatetimeCurrent(){

   var m = moment().tz("2013-06-01T00:00:00", "America/Lima"); //se està restando 5 horas
   var DatetimeCurrent = m.format("YYYY-MM-DD HH:mm:ss");


   //var DatetimeCurrent = moment.tz("2013-11-18T11:55:00","YYYY-MM-DDTHH:mm:ss",true,"America/Lima").format("YYYY-MM-DDTHH:mm:ss");

   return DatetimeCurrent;
}

exports.GetDatetimeCurrent = GetDatetimeCurrent;
exports.RandomName = RandomName;
exports.RandomFileName = RandomFileName;