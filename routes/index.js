const express                = require('express');
const router                 = express.Router();

const BlogAPIController      = require('../controllers/BlogAPIController');
const BlogController         = require('../controllers/BlogController');
const BlogCategoryController         = require('../controllers/BlogCategoryController');

/* BLOG */

//PRIVATE
router.get('/api/blogs', BlogAPIController.List)
      .post('/api/blogs', BlogAPIController.Create)
      .delete('/api/blogs', BlogAPIController.Delete) 
      .put('/api/blogs/:blog_id', BlogAPIController.Update)
      .patch('/api/blogs/:blog_id/photo', BlogAPIController.UpdatePhoto)
      .patch('/api/blogs/:blog_id/state', BlogAPIController.UpdateState)
      .get('/api/blog_categories', BlogCategoryController.List);
      

//PUBLIC
router.get('/blogs', BlogController.List)
      .get('/blogs/:blog_id', BlogController.Show); 

module.exports = router;

