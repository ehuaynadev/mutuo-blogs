use db_mutuo

create database db_mutuo

drop DATABASE db_mutuo


drop table competitions
drop table judges

create table family_photos(
	family_photo_id INT PRIMARY KEY AUTO_INCREMENT,
	photo_original TEXT,
	photo_generated TEXT,
	competition_id INT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (competition_id) REFERENCES competitions (competition_id)	
);

create table competitions(
	competition_id INT PRIMARY KEY AUTO_INCREMENT,	
	title TEXT,
	description TEXT,
	file_image_original TEXT,
	file_image_generated TEXT,
	file_document_original TEXT,
	file_document_generated TEXT,
	amount NUMERIC,	
	user_id_creator INT,
	familia_titulo TEXT,
	familia_description TEXT,
	puesto1_monto NUMERIC,
	puesto1_descripcion TEXT,
	puesto2_monto NUMERIC,
	puesto2_descripcion TEXT,
	puesto3_monto NUMERIC,
	puesto3_descripcion TEXT,
	state enum('INACTIVO','ACTIVO','CERRADO','FINALIZADO') DEFAULT 'INACTIVO',
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (user_id_creator) REFERENCES users (user_id)
)

create table schedules(
	schedule_id INT PRIMARY KEY AUTO_INCREMENT,
	date_apply DATE,
	description TEXT,
	competition_id INT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (competition_id) REFERENCES competitions (competition_id)
)

create table blogs(
	blog_id INT AUTO_INCREMENT PRIMARY KEY,
	title VARCHAR(255),
	description TEXT,
	photo_original TEXT,
	photo_generated TEXT,
	user_creator_id INT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	state enum('BORRADOR','PUBLICADO') DEFAULT 'BORRADOR',
	publication_date TIMESTAMP,
	FOREIGN KEY (user_creator_id) REFERENCES users (user_id)	
)



create table judges(
	judge_id INT PRIMARY KEY AUTO_INCREMENT,	
	name TEXT,
	position TEXT,
	description TEXT,
	photo_original TEXT,
	photo_generated TEXT,	
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)


create table users(
	user_id INT PRIMARY KEY AUTO_INCREMENT,
	username VARCHAR(16),
	person_id INT,
	password VARCHAR(255),
	created_at TIMESTAMP,
	FOREIGN KEY (person_id) REFERENCES persons (person_id)
)


create table persons(
	person_id INT PRIMARY KEY AUTO_INCREMENT,
	lastname VARCHAR(45),
	firstname VARCHAR(45),
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)


create table competitions_link_judges(
	competition_id INT,
	judge_id INT,	
	FOREIGN KEY (competition_id) REFERENCES competitions (competition_id),
	FOREIGN KEY (judge_id) REFERENCES judges (judge_id)
)