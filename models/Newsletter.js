/* 'use strict';
module.exports =  (sequelize, DataTypes ) => {

    var Newsletter = sequelize.define('Newsletter', {
        newsletter_id:{
            type:DataTypes.INTEGER,
            primaryKey:true,
            autoIncrement:true,
            field: 'newsletter_id'
        },
        title: {
            type: DataTypes.STRING,
            //allowNull: false,    
            field: 'title'    
        },    
        file_original_name: {
            type: DataTypes.STRING
        },
        file_generated_name: {
            type: DataTypes.STRING
        },
        user_id: {
            type: DataTypes.STRING
        },
        state:{
            type: DataTypes.INTEGER
        },
        erased:{
            type: DataTypes.INTEGER
        },
        created_at: {
           // allowNull: false,
            type: DataTypes.DATE,
            field: 'created_at',
        },
        updated_at: {
            //allowNull: false,
            type: DataTypes.DATE,
            field: 'updated_at',
        },                
    },{
        freezeTableName: false,
        tableName:'newsletters',
        underscored: false,
        timestamps: false,
    });

  

    return Newsletter;
}; */