require('dotenv').config()

module.exports = { 
    ConnectionAWS : function(){
        return config = {
            endpoint:            process.env.ENDPOINT_IBM_AWS,
            apiKeyId:            process.env.APIKEY_ID,
            ibmAuthEndpoint:     process.env.IBM_AUTH_ENDPOINT,
            serviceInstanceId:   process.env.SERVICE_INSTANCE_ID,
        };
    }
 }

