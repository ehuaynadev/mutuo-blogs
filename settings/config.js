require('dotenv').config()

module.exports = { 
    ConnectionString : function(){
        return db = {
            host:       process.env.DB_HOST,
            user:       process.env.DB_USER,
            password:   process.env.DB_PASS,
            database:   process.env.DB_NAME,
            port:       process.env.DB_PORT,
            multipleStatements: true,
            connectionLimit : 10,
            timezone: 'utc'
            //timezone: "UTC-5"
        };
    }
 }
